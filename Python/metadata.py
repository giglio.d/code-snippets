from functools import wraps

def info(fn):

    @wraps(fn)
    def wrapper(*args, **kwargs)
        """Io sono la funzione wrapper del decoratore"""
        print(f"Stai eseguendo la funzione: {fn.__name__}")
        print(f"La sua documentazione è la seguente: {fn.__doc__}")
        return fn(*args, **kwargs)
    return wrapper

@info
def func():
    """Ïo sono una funzione che non fa nulla""" #docstring
    pass

func()
print(func.__doc__)
print(func.__name__)
        
