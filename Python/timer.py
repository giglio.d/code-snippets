def timer(fn):
    def wrapper(*args, **kwargs):
        from time import perf_counter
        start = perf_counter()
        fn(*args, **kwargs)
        stop = perf_counter()
        print(f"la durata della funzione è {stop - start} secondi")
        return result
    return wrapper

@timer
def func()
    pass
