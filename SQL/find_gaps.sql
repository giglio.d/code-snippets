SELECT
 CONCAT(z.expected, IF(z.got-1>z.expected, CONCAT(' thru ',z.got-1), '')) AS missing
FROM (
 SELECT
  @rownum:=@rownum+1 AS expected,
  IF(@rownum=MyCol, 0, @rownum:=MyCol) AS got
 FROM
  (SELECT @rownum:=0) AS a
  JOIN MyTable
  ORDER BY MyCol
 ) AS z
WHERE z.got!=0;
