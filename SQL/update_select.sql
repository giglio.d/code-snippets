SET SQL_SAFE_UPDATES = 0;

UPDATE responsabili
        INNER JOIN
    utenti ON utenti.cognome = responsabili.cognome
        AND utenti.nome = responsabili.nome 
SET 
    responsabili.username = utenti.username
WHERE
    utenti.cognome = responsabili.cognome
        AND utenti.nome = responsabili.nome;

SET SQL_SAFE_UPDATES = 1;
