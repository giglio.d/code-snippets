Sub AFCopyFilteredRows()
	Dim rng As Range
	Dim ws As Worksheet
	
	If Worksheets("Sheet1").AutoFilterMode = False Then
		MsgBox "There are no filtered rows"
		Exit Sub
	End If
	
	Set rng = Worksheets("Sheet1").AutoFilter.Range
	Set ws = Worksheets.Add
	rng.Copy Range("A1")
End Sub
