Private Sub FilterThenCopy()
   Dim ws, newWS, currentWS As Worksheet
   targetCol = 1   'define which column you want to break
   Dim objDict As Variant
   Set objDict = CreateObject("Scripting.Dictionary")
   Set currentWS = ActiveSheet
   'Add unique value in targetCol to the dictionary
   Application.DisplayAlerts = False
   For r = 2 To Cells(Rows.Count, targetCol).End(xlUp).Row
     If Not objDict.exists(Cells(r, targetCol).Value) Then
       objDict.Add Cells(r, targetCol).Value, Cells(r, targetCol).Value
     End If
   Next r

  If currentWS.AutoFilterMode = True Then
     currentWS.UsedRange.AutoFilter
  End If
  currentWS.UsedRange.AutoFilter
  For Each k In objDict.Keys
    currentWS.UsedRange.AutoFilter field:=1, Criteria1:=objDict.Item(k)
   'delete worksheet if worksheet of item(k) exist
    For Each ws In ActiveWorkbook.Worksheets
      If wsExists(objDict.Item(k)) Then
        Sheets(objDict.Item(k)).Delete
      End If
    Next ws
   'crate worksheet using item(k) name
    Set newWS = ThisWorkbook.Worksheets.Add(After:=Worksheets(Worksheets.Count))
    newWS.Name = objDict.Item(k)
    'copy filtered contents to new worksheet
    currentWS.Cells.Copy
    newWS.Range("A1").Select
    Sheets(objDict.Item(k)).Paste
  Next k
  currentWS.Activate
  currentWS.AutoFilterMode = False
  Application.DisplayAlerts = True
End Sub

Function wsExists(wksName As String) As Boolean
   On Error Resume Next
   wsExists = CBool(Len(Worksheets(wksName).Name) > 0)
   On Error GoTo 0
End Function
