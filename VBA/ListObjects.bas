'selezionare l'intera tabella
Sub SelectTable()
    ActiveSheet.ListObjects("myTable").Range.Select
End Sub

'selezionare solo l'area dei dati
Sub SelectTableData()
    ActiveSheet.ListObjects("myTable").DataBodyRange.Select
End Sub

'estrarre un valora da una cella specifica
Sub GetValueFromTable()
    MsgBox ActiveSheet.ListObjects("myTable").DataBodyRange(2, 4).value
End Sub

'selezionare una colonna per nome o posizione, intestazione compresa
Sub SelectAnEntireColumn()

    'Select column based on position
    ActiveSheet.ListObjects("myTable").ListColumns(2).Range.Select

    'Select column based on name
    ActiveSheet.ListObjects("myTable").ListColumns("Category").Range.Select

End Sub

'selezionare una colonna per nome o posizione (solo dati)
Sub SelectColumnData()

    'Select column data based on position
    ActiveSheet.ListObjects("myTable").ListColumns(4).DataBodyRange.Select

    'Select column data based on name
    ActiveSheet.ListObjects("myTable").ListColumns("Category").DataBodyRange.Select

End Sub

'selezionare l'intestazione di una colonna specifica
Sub SelectCellInHeader()
    ActiveSheet.ListObjects("myTable").HeaderRowRange(5).Select
End Sub

' This example demonstrates how to select the cell in the totals row of the 3rd column.
Sub SelectCellInTotal()
    ActiveSheet.ListObjects("myTable").TotalsRowRange(3).Select
End Sub

' selezionare una riga intera
Sub SelectRowOfData()
    ActiveSheet.ListObjects("myTable").ListRows(3).Range.Select
End Sub

' converte la selezione corrente in una tabella con nome
Sub ConvertRangeToTable()

    tableName As String
    Dim tableRange As Range

    Set tableName = "myTable"
    Set tableRange = Selection.CurrentRegion
    ActiveSheet.ListObjects.Add(SourceType:=xlSrcRange, _
        Source:=tableRange, _
        xlListObjectHasHeaders:=xlYes _
        ).Name = tableName

End Sub

' Tbl 2 Rng
Sub ConvertTableToRange()
    ActiveSheet.ListObjects("myTable").Unlist
End Sub

' Ridefinisce l'area
Sub ResizeTableRange()
    ActiveSheet.ListObjects("myTable").Resize Range("$A$1:$J$100")
End Sub