Private Sub Workbook_SheetChange(ByVal Sh As Object, ByVal Target As Range)
    With Sh
        If Not Intersect(Target, .Columns(1)) Is Nothing Then
            On Error GoTo bm_Safe_Exit
            Application.EnableEvents = False
            Dim rng As Range
            For Each rng In Intersect(Target, .Columns(1))
                rng.Offset(0, 4) = Now
            Next rng
        End If
    End With
bm_Safe_Exit:
    Application.EnableEvents = True
End Sub
