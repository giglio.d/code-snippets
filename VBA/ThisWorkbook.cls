VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
END
Attribute VB_Name = "ThisWorkbook"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = True

Option Explicit
Dim sActiveSheetName As String

Private Sub Workbook_Open()
    ' From the given data source, the "Note" named sheet, we gather column number for each value
    offSrcSampleId = FindColumnOff("Cod_Ana")      '1
    offSrcMethod = FindColumnOff("COD_METODO")
    offSrcParameter = FindColumnOff("Descriz")     '4
    offSrcUnit = FindColumnOff("Un_Mis")           '5
    offSrcValue = FindColumnOff("Valore")          '6
    offSrcACode = FindColumnOff("Cod_T_Ana")       '8
    offSrcRegDate = FindColumnOff("Data_Reg")      '27
    offSrcUncert = FindColumnOff("Incertezza")     '41
End Sub

'This macro automatically fills tables rows when new sample ID is added.
Private Sub Workbook_SheetChange(ByVal Sh As Object, ByVal Target As Range)
    'Set rngActiveCell = ActiveCell
    Dim rInput As Range
    Dim Test As Boolean
    
    With Sh
        If Sh.Index > 4 Then
            sActiveSheetName = Sh.Name
            'In order to properly cast a query it's necessary to have SampleID AND Matrice column filled,
            'method name is guessed from Sheet name.
            'Column containing RdP codes
            If Not Intersect(Target, .ListObjects(1).ListColumns(3).DataBodyRange) Is Nothing Then
                On Error GoTo bm_Safe_Exit
                Application.EnableEvents = False
                For Each rInput In Intersect(Target, .ListObjects(1).ListColumns(3).DataBodyRange)
                    If ((rInput.Value <> "") And (rInput.Offset(0, 1).Value <> "")) Then
                        Debug.Print "Presenti entrambi i valori"
                        Call FillRow(rInput.Value, rInput.Offset(0, 1).Value, rInput.Row)
                    ElseIf (rInput.Offset(0, 1).Value = "") Then
                        Debug.Print "Codice Matrice assente"
                    ElseIf (rInput.Value = "") Then
                        Debug.Print "Codice RdP assente"
                    End If
                Next rInput
            End If
            
            'Column containing Matrix type
            If Not Intersect(Target, .ListObjects(1).ListColumns(4).DataBodyRange) Is Nothing Then
                On Error GoTo bm_Safe_Exit
                Application.EnableEvents = False
                For Each rInput In Intersect(Target, .ListObjects(1).ListColumns(4).DataBodyRange)
                    If ((rInput.Value <> "") And (rInput.Offset(0, -1).Value <> "")) Then
                        Debug.Print "Presenti entrambi i valori"
                        Call FillRow(rInput.Offset(0, -1).Value, rInput.Value, rInput.Row)
                    ElseIf (rInput.Offset(0, -1).Value = "") Then
                        Debug.Print "Codice RdP assente"
                    ElseIf (rInput.Value = "") Then
                        Debug.Print "Codice Matrice assente"
                    End If
                Next rInput
            End If
        End If
    End With
Done:
    Application.EnableEvents = True
    Exit Sub
bm_Safe_Exit:
    Application.EnableEvents = True
    Debug.Print Err.Number, Err.Description
End Sub

Private Function Validate(strInput As String, Optional Flag As String = "") As Boolean
    Dim regex As Object, str As String
    Set regex = CreateObject("VBScript.RegExp")

    With regex
        .Pattern = "2[0-9]{6}-[0-9]{3}"
    End With
    
    If Flag = "RDP" Then
        Validate = regex.Test(strInput) 'Result: True
        'Debug.Print "Validazione del SampleID"
    ElseIf Flag = "MATYPE" Then
        Validate = True 'Result: True
        'Debug.Print "Validazione del tipo matrice"
    End If
End Function

Private Sub FillRow(strSampleID As String, strMatrixType As String, lCurrentRow As Long)
    Dim blError As Boolean
    Dim lRowIndex As Long
    
    If Not (Validate(strSampleID, "RDP")) Then
        Debug.Print "Codice RdP non conforme"
        Exit Sub
    Else
        Debug.Print "Codice RdP conforme"
    End If
    
    If Not (Validate(strMatrixType, "MATYPE")) Then
        Debug.Print "Tipo di matrice non conforme"
        Exit Sub
    Else
        Debug.Print "Tipo di matrice conforme"
    End If
    
    If TableExistsOnSheet(ActiveSheet, "DataPool") Then
        lRowIndex = LookFor(strSampleID, strMatrixType)
        Call CopyCells(lRowIndex, lCurrentRow)
    Else
        MsgBox "La tabella DataPool non esiste o non è stata rinominata"
    End If
End Sub

Private Function LookFor(strSampleID As String, strMatrixType As String)
    Dim sCodMethod As String
    Dim rResSet As Range, FindValueInTable As Range
    Dim DataPool As Object
    
    Set DataPool = Sheets("Note").ListObjects("DataPool")
    DataPool.AutoFilter.ShowAllData
    
    'Applies range filter
    Select Case sActiveSheetName
        Case "ISO 9696"
            With DataPool.DataBodyRange
                .AutoFilter field:=offSrcMethod, Criteria1:="ISO 9696*"
                .AutoFilter field:=offSrcACode, Criteria1:=strMatrixType
            End With
        Case "ISO 9697"
            With DataPool.DataBodyRange
                .AutoFilter field:=offSrcMethod, Criteria1:="ISO 9697*"
                .AutoFilter field:=offSrcACode, Criteria1:=strMatrixType
            End With
        Case "ISO 9698"
            With DataPool.DataBodyRange
                .AutoFilter field:=offSrcMethod, Criteria1:="ISO 9698*"
                .AutoFilter field:=offSrcACode, Criteria1:=strMatrixType
            End With
        Case "ISO 10703 Cs"
            DataPool.DataBodyRange.AutoFilter field:=offSrcMethod, Criteria1:="ISO 10703*"
            DataPool.DataBodyRange.AutoFilter field:=offSrcParameter, Criteria1:="Cs-137"
            DataPool.DataBodyRange.AutoFilter field:=offSrcACode, Criteria1:=strMatrixType
        Case "ISO 10703 Co"
            DataPool.DataBodyRange.AutoFilter field:=offSrcMethod, Criteria1:="ISO 10703*"
            DataPool.DataBodyRange.AutoFilter field:=offSrcParameter, Criteria1:="Co-60"
            DataPool.DataBodyRange.AutoFilter field:=offSrcACode, Criteria1:=strMatrixType
        Case "ISO 10703 Am"
            DataPool.DataBodyRange.AutoFilter field:=offSrcMethod, Criteria1:="ISO 10703*"
            DataPool.DataBodyRange.AutoFilter field:=offSrcParameter, Criteria1:="Am-241"
            DataPool.DataBodyRange.AutoFilter field:=offSrcACode, Criteria1:=strMatrixType
        Case "ISO 10704 Alfa"
            DataPool.DataBodyRange.AutoFilter field:=offSrcMethod, Criteria1:="ISO 10704*"
            DataPool.DataBodyRange.AutoFilter field:=offSrcParameter, Criteria1:="Alfa totale"
            DataPool.DataBodyRange.AutoFilter field:=offSrcACode, Criteria1:=strMatrixType
        Case "ISO 10704 Beta"
            DataPool.DataBodyRange.AutoFilter field:=offSrcMethod, Criteria1:="ISO 10704*"
            DataPool.DataBodyRange.AutoFilter field:=offSrcParameter, Criteria1:="Beta totale"
            DataPool.DataBodyRange.AutoFilter field:=offSrcACode, Criteria1:=strMatrixType
        Case "MIP LMR-02 Sr"
            With DataPool.DataBodyRange
                .AutoFilter field:=offSrcMethod, Criteria1:="MIP 02*"
                .AutoFilter field:=offSrcACode, Criteria1:=strMatrixType
            End With
        Case "MIP LMR-10"
            DataPool.DataBodyRange.AutoFilter field:=offSrcMethod, Criteria1:="MIP 10*"
            DataPool.DataBodyRange.AutoFilter field:=offSrcACode, Criteria1:=strMatrixType
        Case Else
            DataPool.DataBodyRange.AutoFilter field:=offSrcMethod, Criteria1:=sActiveSheetName
            DataPool.DataBodyRange.AutoFilter field:=offSrcACode, Criteria1:=strMatrixType
    End Select

    'Set rResSet = Sheets("Note").ListObjects("DataPool").DataBodyRange.Find(What:=strSampleID, LookIn:=xlFormulas, LookAt:=xlWhole)
    Set rResSet = DataPool.DataBodyRange.Find(What:=strSampleID, LookIn:=xlFormulas, LookAt:=xlWhole)
    If rResSet Is Nothing Then
        Debug.Print "Sample ID not found"
        LookFor = 0
    Else
        Debug.Print "il campione " & strSampleID & " è nella riga " & rResSet.Row
        LookFor = rResSet.Row
    End If
    
    DataPool.AutoFilter.ShowAllData
End Function

Private Sub CopyCells(lRowIndex As Long, lCurrentRow As Long)
    Dim DataPool As Object

    Set DataPool = Sheets("Note").ListObjects("DataPool")
    DataPool.AutoFilter.ShowAllData
    
    If lRowIndex = 0 Then
        Exit Sub
    End If
    
    'date
    Worksheets(sActiveSheetName).Cells(lCurrentRow, offDstRegDate) = Left(Worksheets("Note").Cells(lRowIndex, offSrcRegDate).Value, 10)
    Debug.Print "registrazione: " & Worksheets("Note").Cells(lRowIndex, offSrcRegDate).Value
    'descrizione
    Worksheets(sActiveSheetName).Cells(lCurrentRow, offDstParameter) = Worksheets("Note").Cells(lRowIndex, offSrcParameter).Value
    Debug.Print "parametro: " & Worksheets("Note").Cells(lRowIndex, offSrcParameter).Value
    'unita di misura
    Worksheets(sActiveSheetName).Cells(lCurrentRow, offDstUnit) = Worksheets("Note").Cells(lRowIndex, offSrcUnit).Value
    Debug.Print "unita: " & Worksheets("Note").Cells(lRowIndex, offSrcUnit).Value
    'valore
    Worksheets(sActiveSheetName).Cells(lCurrentRow, offDstValue) = Worksheets("Note").Cells(lRowIndex, offSrcValue).Value
    Debug.Print "valore: " & Worksheets("Note").Cells(lRowIndex, offSrcValue).Value
    'Worksheets(sActiveSheetName).Cells(lCurrentRow, offDstACode) = Worksheets("Note").Cells(lRowIndex, offSrcACode).Value
    'Worksheets(sActiveSheetName).Cells(lCurrentRow, offDstParameter)
    Worksheets(sActiveSheetName).Cells(lCurrentRow, offDstUncert) = IIf(StrComp(Worksheets("Note").Cells(lRowIndex, offSrcUncert).Value, "/"), _
                                            Replace(Worksheets("Note").Cells(lRowIndex, offSrcUncert).Value, ",", "."), _
                                            0#)
    Debug.Print "incertezza: " & Worksheets("Note").Cells(lRowIndex, offSrcUncert).Value & "; formato: " & Worksheets("Note").Cells(lRowIndex, offSrcUncert).NumberFormat
End Sub

