' -*- major-mode: visual-basic -*-
Option Explicit

Dim offSrcSampleId As Integer, offSrcACode As Integer, offSrcParameter As Integer, _
    offSrcUnit As Integer, offSrcValue As Integer, _
    offSrcUncert As Integer, offSrcRegDate As Integer

Dim offDstACode As Integer, offDstParameter As Integer, _
    offDstUnit As Integer, offDstValue As Integer, _
    offDstUncert As Integer, offDstRegDate As Integer

Dim startingRow As Integer

Type tSample
    Id As String
    Parameter As String
    Unit As String
    Value As Double
    Uncertainity As Double
    AnalysisCode As Single
    RegDate As Date
End Type

Sub FillReport()
    
Dim lr As Long, botRowSrc As Long, dstRow As Integer
Dim Sample As Range, Pool As Range, Entry As Range
Dim tmpRange As Range
Dim firstAddress As String
Dim SampleIds() As String
Dim idx As Integer, iCurrent As Integer, i As Integer ', j As Integer, k As Integer
Dim lastWkSht As Integer
Dim wshName As String, SampleId As String
Dim wsTemp As Worksheet

' variables initialization
' source cells
lastWkSht = Worksheets.Count
botRowSrc = Sheets("Note").Cells(Rows.Count, 3).End(xlUp).Row

' From the given data source, the "Note" named sheet, we gather column number for each value
offSrcSampleId = FINDOFFSET("Cod_Ana")      '1
offSrcParameter = FINDOFFSET("Descriz")     '4
offSrcUnit = FINDOFFSET("Un_Mis")           '5
offSrcValue = FINDOFFSET("Valore")          '6
offSrcACode = FINDOFFSET("Cod_T_Ana")       '8
offSrcRegDate = FINDOFFSET("Data_Reg")      '27
offSrcUncert = FINDOFFSET("Incertezza")     '41

'destination cells
offDstRegDate = 2
offDstACode = 4
offDstParameter = 6
offDstUnit = 7
offDstValue = 10
offDstUncert = 11
' variables initialization -- END

' in case of need to clean up the source pool
'With Rng
'    .Replace "NULL", ""
'End With
'Sheets("Note").Activate

'lr = Worksheets(4).Cells(Rows.Count, 3).End(xlUp).Row

'DEBUG
Application.DisplayAlerts = False
For iCurrent = 13 To 13
'For iCurrent = 5 To lastWkSht
    Sheets("Note").AutoFilterMode = False
    
    ' gets sheet name
    wshName = Worksheets(iCurrent).Name
    
    ' gets the samples ID to be looked for
    startingRow = FINDSTARTINGROW(iCurrent)
    ' Sample.Id = Worksheets(Count).Cells(startingRow, 3)
    lr = Worksheets(iCurrent).Cells(Rows.Count, 3).End(xlUp).Row
    
    ' Checks if the current sheet has any rows to be filled
    If lr >= startingRow Then
    
    ' Searching for every sample using parameter wshName
        Set Pool = GETROWSLIST(wshName, Sheets("Note").Range("A6:" & "CQ" & botRowSrc))
        
        If Not Pool Is Nothing Then
            ' lavorare direttamente sul range prodotto da AutoFilter non è semplice
            ' ho preferito create un foglio temporaneo in coda agli altri
            Set wsTemp = ThisWorkbook.Worksheets.Add(After:=Worksheets(Worksheets.Count))
            wsTemp.Name = "TEMP"
            Pool.Copy wsTemp.Range("A1")
            Worksheets(iCurrent).Activate
            If wsTemp.Rows.Count = 1 Then
                Call CopyAndPasteRow(wsTemp.UsedRange, iCurrent, startingRow, lr)
            End If
            wsTemp.Delete
        End If
    
    End If
Next iCurrent

Application.DisplayAlerts = True
End Sub

Private Sub CopyAndPasteRow(rSrcPool As Range, iCurrent As Integer, _
                            startRow As Integer, endRow As Long)

    ' vogliamo estrarre dal range filtrato per metodo solo i record
    ' che corrispondono ai codici campione del foglio risultati
    ' Dim VSrcRow As Variant
    Dim iRow As Integer, iDstRow As Integer
    Dim sSampleID As String
    Dim rResSet As Range
    
    'rSrcPool.Select
    'Debug.Print rSrcPool.Rows.Count
    'Debug.Print rSrcPool.Columns.Count
    'Dopo aver filtrato in base al metodo il pool di dati grezzi,
    'bisogna estrarre solo i valori dei campioni scelti per la valutazione.
        
    'Once the source pool of samples results has been delimited we operate on it
    For iRow = startRow To endRow
        sSampleID = Sheets(iCurrent).Cells(iRow, 3).Value
        'iDstRow = Sheets(iCurrent).Range("C" & startRow & ":C" & endRow).Find(VSrcRow.Offset(0, -2).Value, LookIn:=xlFormulas, LookAt:=xlWhole)
        'Set rResSet = rSrcPool("A:A").Find(sSampleID, LookIn:=xlFormulas, LookAt:=xlWhole)
        Set rResSet = rSrcPool.Find(sSampleID, LookIn:=xlFormulas, LookAt:=xlWhole)
        'Sheets("Note").Cells(rResSet.Row, offSrcValue).Val
        'ActiveSheet.Cells(rResSet.Row, offSrcValue).Select
    
        'With rSrcPool
            'Sheets("Note").Activate
        '    .Cells(rResSet.Row, offSrcRegDate).Select
        '    .Cells(rResSet.Row, offSrcParameter).Select
        '    .Cells(rResSet.Row, offSrcUnit).Select
        '    .Cells(rResSet.Row, offSrcValue).Select
        'End With
        
        With Worksheets(iCurrent)
            .Cells(iRow, 1) = "TEST"
            .Cells(iRow, offDstRegDate) = Left(rSrcPool.Cells(rResSet.Row, offSrcRegDate).Value, 10)
            .Cells(iRow, offDstParameter) = rSrcPool.Cells(rResSet.Row, offSrcParameter).Value
            .Cells(iRow, offDstUnit) = rSrcPool.Cells(rResSet.Row, offSrcUnit).Value
            .Cells(iRow, offDstValue) = rSrcPool.Cells(rResSet.Row, offSrcValue).Value
            .Cells(iRow, offDstACode) = rSrcPool.Cells(rResSet.Row, offSrcACode).Value
            '.Cells(iRow, offDstUncert) = iIf VBA.IsNumeric(rSrcPool.Cells(rResSet.Row, offSrcUncert).Value) Then
            '    .Cells(iRow, offDstUncert) = rSrcPool.Cells(rResSet.Row, offSrcUncert).Value
            .Cells(iRow, offDstUncert) = IIf(StrComp(rSrcPool.Cells(rResSet.Row, offSrcUncert).Value, "/"), _
                                             0#, _
                                             rSrcPool.Cells(rResSet.Row, offSrcUncert).Value)
        End With
        
    Next iRow
    
End Sub

Private Sub CopyAndPasteRow_OLD(rSrc As Integer, rDst As Integer, idxDstWs As Integer)
    ' this routine, given a row number, copies sensible values from the right source cell
    ' to the right destination cell.
    Dim Sample As tSample
    Dim test As Boolean

    ' Copying
    With Worksheets("Note")
        Sample.Parameter = .Cells(rSrc, offSrcParameter).Value
        Sample.Unit = .Cells(rSrc, offSrcUnit).Value
        Sample.Value = .Cells(rSrc, offSrcValue).Value
        Sample.AnalysisCode = .Cells(rSrc, offSrcACode).Value
        Sample.RegDate = Left(.Cells(rSrc, offSrcRegDate).Value, 10)
        'Sample.Uncertainity = VBA.IIf(.Cells(rSrc, offSrcUncert).Value = "/", 0#, _
        '                              .Cells(rSrc, offSrcUncert).Value)
        'test = VBA.IsNumeric(.Cells(rSrc, offSrcUncert).Value)
        If VBA.IsNumeric(.Cells(rSrc, offSrcUncert).Value) Then
            Sample.Uncertainity = .Cells(rSrc, offSrcUncert).Value
        End If
    End With
  
    ' Pasting
    With Worksheets(idxDstWs)
        .Cells(rDst, offDstRegDate) = Sample.RegDate
        .Cells(rDst, offDstACode) = Sample.AnalysisCode
        .Cells(rDst, offDstACode).Font.Color = vbRed
        .Cells(rDst, offDstParameter) = Sample.Parameter
        .Cells(rDst, offDstUnit) = Sample.Unit
        .Cells(rDst, offDstValue) = Sample.Value
        .Cells(rDst, offDstUncert) = Sample.Uncertainity
    End With
    
End Sub

Private Function FINDSTARTINGROW(wshIdx) As Integer
    Dim lCell As Range, Rng As Range, lRow As Integer
    Dim ws As Worksheet
    Dim tbl As ListObject
    
    Set ws = Worksheets(wshIdx)
    
    Set tbl = ws.ListObjects(1)
    With tbl.ListColumns(1).Range
        lRow = .Find(What:="*", _
                     After:=.Cells(1), _
                     LookAt:=xlPart, _
                     LookIn:=xlFormulas, _
                     SearchOrder:=xlByRows, _
                     SearchDirection:=xlPrevious, _
                     MatchCase:=False).Row
    End With
    
    FINDSTARTINGROW = lRow + 1
End Function

Private Function FINDOFFSET(Colname As String) As Integer
    Dim c As Range
    
    FINDOFFSET = Sheets("Note").Rows(5).Find(What:=Colname, _
                                             LookIn:=xlFormulas, _
                                             LookAt:=xlWhole).Column
End Function

Private Function GETROWSLIST(LookFor As String, rngLookIn As Range) As Range
    ' This function applies, on the raw data pool, an autofilter based on the sheet name which specifies the method used.
    ' Some sheets collect results specific for sub types (i.e. alfa/beta) thus we need to manage speciale cases.
    Dim firstAddress As String, Record As Range, RetSet As Range
    
    Sheets("Note").AutoFilterMode = False
    
    ' some searches require to look for on more than one single cell, so these
    ' events have to be checked.
    Select Case LookFor
        Case "ISO 10704 Alfa"
            Sheets("Note").Range("A5:CQ5").AutoFilter field:=3, Criteria1:="ISO 10704"
            Sheets("Note").Range("A5:CQ5").AutoFilter field:=4, Criteria1:="Alfa totale"
            Set RetSet = Sheets("Note").AutoFilter.Range
        Case "ISO 10704 Beta"
            Sheets("Note").Range("A5:CQ5").AutoFilter field:=3, Criteria1:="ISO 10704"
            Sheets("Note").Range("A5:CQ5").AutoFilter field:=4, Criteria1:="Beta totale"
            Set RetSet = Sheets("Note").AutoFilter.Range
        Case "ISO 10703 Cs"
            Sheets("Note").Range("A5:CQ5").AutoFilter field:=3, Criteria1:="ISO 10703"
            Sheets("Note").Range("A5:CQ5").AutoFilter field:=4, Criteria1:="Cs-137"
            Set RetSet = Sheets("Note").AutoFilter.Range
        Case "ISO 10703 Co"
            Sheets("Note").Range("A5:CQ5").AutoFilter field:=3, Criteria1:="ISO 10703"
            Sheets("Note").Range("A5:CQ5").AutoFilter field:=4, Criteria1:="Co-60"
            Set RetSet = Sheets("Note").AutoFilter.Range
        Case "ISO 10703 Am"
            Sheets("Note").Range("A5:CQ5").AutoFilter field:=3, Criteria1:="ISO 10703"
            Sheets("Note").Range("A5:CQ5").AutoFilter field:=4, Criteria1:="Am-241"
            Set RetSet = Sheets("Note").AutoFilter.Range
        Case Else
            Sheets("Note").Range("A5:CQ5").AutoFilter field:=3, Criteria1:=LookFor
            Set RetSet = Sheets("Note").AutoFilter.Range
    End Select
    
    GoTo DEBUG1
    If InStr(LookFor, "ISO 10704") Then
        Sheets("Note").Range("A5:CQ5").AutoFilter field:=3, Criteria1:="ISO 10704"
        If InStr(LookFor, "Alfa") Then
            Sheets("Note").Range("A5:CQ5").AutoFilter field:=4, Criteria1:="Alfa totale"
            Set RetSet = Sheets("Note").AutoFilter.Range
        Else 'Beta totale
            Sheets("Note").Range("A5:CQ5").AutoFilter field:=4, Criteria1:="Beta totale"
            Set RetSet = Sheets("Note").AutoFilter.Range
        End If
    Else
        Sheets("Note").Range("A5:CQ5").AutoFilter field:=3, Criteria1:=LookFor
        Set RetSet = Sheets("Note").AutoFilter.Range
    End If
    
    With rngLookIn
        'se la stringa di ricerca è ISO 10704 alfa/beta dobbiamo scomporre la stringa e fare una ricerca condizionata
        Set Record = .Find(LookFor, LookIn:=xlValues, LookAt:=xlWhole)
        If Not Record Is Nothing Then
            firstAddress = Record.Address
            Set RetSet = Record
            'Set RetSet = Application.Union(RetSet, Record)
            Do
                Set Record = .FindNext(Record)
                If Not Record Is Nothing Then Set RetSet = Application.Union(RetSet, Record)
            Loop While Not Record Is Nothing And Record.Address <> firstAddress
        End If
    End With
    
DEBUG1:
    
    Set GETROWSLIST = RetSet
    
End Function
