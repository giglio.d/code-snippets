' lists the sheets that compose the WorkBook
Sub ListSheets()
 
Dim ws As Worksheet
Dim x As Integer
 
x = 1
 
Sheets("Sheet1").Range("A:A").Clear
 
For Each ws In Worksheets
     Sheets("Sheet1").Cells(x, 1) = ws.Name
     x = x + 1
Next ws
 
End Sub

' trova tutte le tabelle presenti che foglio corrente
Sub FindAllTablesOnSheet()
    Dim oSh As Worksheet
    Dim oLo As ListObject
    Set oSh = ActiveSheet
    For Each oLo In oSh.ListObjects
        Application.Goto oLo.Range
        MsgBox "Table found: " & oLo.Name & ", " & oLo.Range.Address
    Next
End Sub