Sub RemovePartsOfTable()

Dim tbl As ListObject

Set tbl = ActiveSheet.ListObjects("Table1")

'Remove 3rd Column
  tbl.ListColumns(3).Delete

'Remove 4th DataBody Row
  tbl.ListRows(4).Delete

'Remove 3rd through 5th DataBody Rows
  tbl.Range.Rows("3:5").Delete

'Remove Totals Row
  tbl.TotalsRowRange.Delete

End Sub

Sub ResetTable()

Dim tbl As ListObject

Set tbl = ActiveSheet.ListObjects("Table1")

'Delete all table rows except first row
  With tbl.DataBodyRange
    If .Rows.Count > 1 Then
      .Offset(1, 0).Resize(.Rows.Count - 1, .Columns.Count).Rows.Delete
    End If
  End With

'Clear out data from first table row
  tbl.DataBodyRange.Rows(1).ClearContents

End Sub

Sub ResetTable()

Dim tbl As ListObject

Set tbl = ActiveSheet.ListObjects("Table1")

'Delete all table rows except first row
  With tbl.DataBodyRange
    If .Rows.Count > 1 Then
      .Offset(1, 0).Resize(.Rows.Count - 1, .Columns.Count).Rows.Delete
    End If
  End With

'Clear out data from first table row (retaining formulas)
  tbl.DataBodyRange.Rows(1).SpecialCells(xlCellTypeConstants).ClearContents

End Sub

Sub LoopingThroughTable()

Dim tbl As ListObject
Dim x As Long

Set tbl = ActiveSheet.ListObjects("Table1")

'Loop Through Each Column in Table
  For x = 1 To tbl.ListColumns.Count
    tbl.ListColumns(x).Range.ColumnWidth = 8
  Next x

'Loop Through Every Row in Table
  For x = 1 To tbl.Range.Rows.Count
    tbl.Range.Rows(x).RowHeight = 20
  Next x
  
'Loop Through Each DataBody Row in Table
  For x = 1 To tbl.ListRows.Count
    tbl.ListRows(x).Range.RowHeight = 15
  Next x

End Sub

Sub LookupTableValue()
'The following code looks to find and ID string within a specific
'table’s first column and returns that ID’s table row number.
Dim tbl As ListObject
Dim FoundCell As Range
Dim LookupValue As String

'Lookup Value
  LookupValue = "ID-123"

'Store Table Object to a variable
  Set tbl = ActiveSheet.ListObjects("Table1")

'Attempt to find value in Table's first Column
  On Error Resume Next
  Set FoundCell = tbl.DataBodyRange.Columns(1).Find(LookupValue, LookAt:=xlWhole)
  On Error GoTo 0

'Return Table Row number if value is found
  If Not FoundCell Is Nothing Then
    MsgBox "Found in table row: " & _
      tbl.ListRows(FoundCell.Row - tbl.HeaderRowRange.Row).Index
  Else
    MsgBox "Value not found"
  End If

End Sub

Sub SingleColumnTable_To_Array()
' How to load in data from a single column and a multi column table
Dim myTable As ListObject
Dim myArray As Variant
Dim TempArray As Variant
Dim x As Long

'Set path for Table variable
  Set myTable = ActiveSheet.ListObjects("Table1")

'Create Array List from Table
  TempArray = myTable.DataBodyRange
  
'Convert from vertical to horizontal array list
  myArray = Application.Transpose(TempArray)

'Loop through each item in the Table Array (displayed in Immediate Window [ctrl + g])
  For x = LBound(myArray) To UBound(myArray)
    Debug.Print myArray(x)
  Next x
  
End Sub

Sub MultiColumnTable_To_Array()

Dim myTable As ListObject
Dim myArray As Variant
Dim x As Long

'Set path for Table variable
  Set myTable = ActiveSheet.ListObjects("Table1")

'Create Array List from Table
  myArray = myTable.DataBodyRange

'Loop through each item in Third Column of Table (displayed in Immediate Window [ctrl + g])
  For x = LBound(myArray) To UBound(myArray)
    Debug.Print myArray(x, 3)
  Next x
  
End Sub