(defun frame-font-setup
    (&rest ...)
  ;; (remove-hook 'focus-in-hook #'frame-font-setup)
  (unless (assoc 'font default-frame-alist)
    (let* ((font-family (catch 'break
                          (dolist (font-family
                                   '("Powerline Consolas"
                                     "Consolas for Powerline"
                                     "Consolas"
                                     ;;
                                     "Powerline Inconsolata-g"
                                     "Inconsolata-g for Powerline"
                                     "Inconsolata-g"
                                     ;;
                                     "Powerline Source Code Pro"
                                     "Source Code Pro for Powerline"
                                     "Source Code Pro"
                                     ;;
                                     "Powerline DejaVu Sans Mono"
                                     "DejaVu Sans Mono for Powerline"
                                     "DejaVu Sans Mono"
                                     ;;
                                     "Monospace"))
                            (when (member font-family (font-family-list))
                              (throw 'break font-family)))))
           (font (when font-family (format "%s-12" font-family))))
      (when font
        (add-to-list 'default-frame-alist (cons 'font font))
        (set-frame-font font t t)))))
(add-hook 'focus-in-hook #'frame-font-setup)